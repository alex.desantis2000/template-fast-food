let deliveryWrapped = document.querySelector('#deliveryWrapped');

const delivery = {

    deliveries : [

        {image : './images/bag-delivery.avif', title : 'Lorem bag delivery', subtitle : 'Maiores rem veritatis mollitia. Unde soluta neque iste reiciendis est adipisci, maxime itaque id fugit, ut a. Quis amet placeat molestias vel!'},

        {image : './images/food-delivery.avif', title : 'Lorem food delivery', subtitle : 'Quis sint corporis, unde quaerat facere eaque ipsum dolore officia nemo ut earum ea necessitatibus voluptatem fugiat incidunt veritatis? Quam, officiis sapiente!'}
    ],

    showDeliveries : function(){

        this.deliveries.forEach( (oneparagraph) => {

            let div = document.createElement('div');

            div.classList.add('title-delivery');

            div.innerHTML = `
            
            <img src="${oneparagraph.image}" alt="delivery-man">

            <h2>${oneparagraph.title}</h2>

            <p>${oneparagraph.subtitle}</p>
            
            `

            deliveryWrapped.appendChild(div);
        })
    }
}

delivery.showDeliveries();